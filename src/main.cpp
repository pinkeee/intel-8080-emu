#include <iostream>
#include <fstream>

#include "8080.hpp"

// Here just so I am able to compile and syntax check, this is subject to change.
int main(int args, char *argv[])
{    
    if(!argv[1])
    {
        std::cout << "Please run program with the ROM as an arg. Such as ./out roms/TEST.COM" << std::endl;

        return EXIT_FAILURE;
    }

    ChipData* i8080 = new ChipData(argv[1]);

    i8080->emulate_cycle();

    return EXIT_SUCCESS;
}
