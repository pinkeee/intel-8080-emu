// https://en.wikipedia.org/wiki/Intel_8080
#pragma once

#include <iostream>
#include <fstream>
#include <array>

using int8 = uint8_t;
using int16 = uint16_t;

class ChipData
{
private:
    // 16bit stack pointer
    int16 sp;

    // 16bit program counter
    int16 pc;

    // 16bit cycle counter
    int16 cycle;

    // 16bit opcode var
    int16 opcode;

    // memory array
    std::array<int16,0xFFFF>memory;

    // registers
    int16 regBC;
    int16 regDE;
    int16 regHL;
    
    int8* regM;
    int8* regA;
    int8* regB = ((int8*) &regBC) + 1;
    int8* regC = ((int8*) &regBC);
    int8* regD = ((int8*) &regDE) + 1;
    int8* regE = ((int8*) &regDE);
    int8* regH = ((int8*) &regHL) + 1;
    int8* regL = ((int8*) &regDE);

    // flags 
    int8 s; // sign flag
    int8 z; // zero flag
    int8 p; // parity flag
    int8 h; // half-carry flag
    int8 c; // carry flag
    int8 ac; // auxillary carry

    bool halt;

    // reset vars used in emu
    void emu_init(void);

    // load rom
    bool load_rom(std::string);

    // get byte of data from memory using PC as adress
    int16 GrabByte(int16);

    // push what ever val is in second parameter onto memory at address of first parameter
    void PushByte(int16, int16); 

    // helpful function to save me time from writing it all out again and again.
    // thank you https://github.com/MoonfireSeco/Intel-8080-Emulator/blob/master/CPU.cpp#L132 :)
    void PushWord(int16, int16);

    // read word at address
    int16 ReadWord(int16);

    // pushes value of parameter onto stack at stack pointer minus two
    void PushOnStack(int16);

    // pops reg a and flags
    void popPSW(void);

    // push psw onto stack
    void pushPSW(void);

    // call function at
    void CALL(void);

    // set parity flag
    bool Parity(int16);

    // set zsp function
    void SETZSP(int8);

    // carry function
    int8 CARRY(int8,int8,int8,int16);

    // for INR function (so i dont have to keep writing out the SAME thing over and over)
    void INR(int8);

    // same as INR but sub instead of add
    void DCR(int8); 

    // ADD function
    void ADD(int8,int8,int8);

    // SUB function
    void SUB(int8,int8,int8);

    // ANA function, this will preform a bitwise AND function on regA and the argument specified
    void ANA(int8);

    // XRA function, bitwise XOR
    void XRA(int8); 

    // ORA fucntion, bitwise OR
    void ORA(int8); 

    // pop function, restores whatever is inside the stack at address into register 
    int16 pop(void);

    // read the next word in the stack
    int16 ReadNextWord(void);

public:
    // things for SDL2 (GUI tool)    
    
    // emulate one CPU cycle
    void emulate_cycle(void);

    // constructor
    ChipData(std::string);

    // deconstructor
    ~ChipData();

};