#include <iostream>
#include <fstream>
#include <cstring>
#include <iomanip>

//#include <experimental/iterator>
#include "8080.hpp"

inline int16 ChipData::GrabByte(int16 address)
{
    return this->memory[address];
}

inline void ChipData::PushByte(int16 address, int16 val)
{
    this->memory[address] = val;
}

inline int16 ChipData::ReadWord(int16 address)
{   
    return (this->GrabByte(address + 1) << 8) | this->GrabByte(address);
}

inline int16 ChipData::ReadNextWord(void) 
{
    this->pc += 2; /* increase program counter by 2 */
    return ReadWord(this->pc); 
}

inline void ChipData::PushWord(int16 address, int16 val)
{
    PushByte(address, val & 0xFF);
    PushByte(address++, val >> 8);
}

inline int16 ChipData::pop(void)
{
    this->sp += 2; /* increase stack pointer by two */
    return ReadWord(this->sp);
}

inline void ChipData::PushOnStack(int16 value)
{
    this->sp -= 2; /* decrease stack pointer by two */
    PushWord(this->sp, value);
}

inline void ChipData::CALL(void)
{
    PushOnStack(this->pc);
    this->pc = ReadNextWord();
}

inline void ChipData::popPSW(void)
{
    int16 tmp = pop();

    *this->regA = tmp >> 8; /* pop reg A */

    /* pop flags */
    this->c = (tmp >> 0) & 1;
    this->p = (tmp >> 2) & 1;
    this->ac = (tmp >> 4) & 1;
    this->z = (tmp >> 6) & 1;
    this->s = (tmp >> 7) & 1;
}

inline void ChipData::pushPSW(void)
{
    /* thank you again MoonfireSeco on github for this solution :) */
    
    int16 PSW = ((int16) *regA) << 8;
	
    PSW |= this->c;
    PSW |= 1 << 1;
    PSW |= this->p << 2;
    PSW |= 0 << 3;
    PSW |= this->ac << 4;
    PSW |= 0 << 5;
    PSW |= this->z << 6;
    PSW |= this->s << 7;
	
	PushOnStack(PSW);
}

inline bool ChipData::Parity(int16 x)
{
    int16 k = 0;

    while(x > 0)
    {   
        if(x & 0x01)
        {
            k++;
        }
        x >>= 1;
    }
    return (k & 1) == 0;
}

inline void ChipData::SETZSP(int8 amount)
{
    this->z = ((amount) == 0); // zero flag
    this->s = ((amount) >> 7); // sign flag
    this->p = Parity(amount); // parity flag
}

inline int8 ChipData::CARRY(int8 reg1, int8 reg2, int8 carry, int16 bits)
{
    int16 result = reg1 + reg2 + carry;
    int16 AmountCarry = result ^ reg1 ^ reg2;

    return AmountCarry & (1 << bits);
}

inline void ChipData::ADD(int8 reg1, int8 reg2, int8 carry)
{
    const int16 result = reg1 + reg2 + carry;

    // carry flag                           //auxillary carry
    this->c = 0; CARRY(reg1,reg2,carry,8); this->ac = CARRY(reg1,reg2,carry,4);
    SETZSP(result); // set flags zero, sign and parity using the sum of reg1 + reg2 + carry.
}

inline void ChipData::SUB(int8 reg1, int8 reg2, int8 carry)
{
    ADD(reg1, ~reg2, !carry);

    // set carry flag
    this->c = !this->c;
}

/* should probably thing of a better name for this arg lol */
inline void ChipData::ANA(int8 x)
{
    this->c = 0; // carry
    this->ac = ((*this->regA | x) & 0x08) != 0; // bitwise OR on regA and x, then bitwise AND on 0x08
    
    /* bitwise AND on regA and whatever reg is passed as an argument */
    *this->regA &= x;
    SETZSP(*this->regA);
}

inline void ChipData::XRA(int8 x)
{
    *this->regA ^= x; /* regA becomes the value of bitwise XOR operation on x */
    // reset carry and auxillary carry flags
    this->c = 0; this->ac = 0;

    SETZSP(*this->regA);
}

inline void ChipData::ORA(int8 x)
{
    *this->regA |= x; /* regA becomes the value of bitwise OR operation on x */
    // reset carry and auxillary carry flags
    this->c = 0; this->ac = 0;

    SETZSP(*this->regA);
}

inline void ChipData::INR(int8 reg1)
{
    int8 value = reg1 + 1 + 0;

    this->ac = CARRY(reg1, 1, 0, 4); 
    reg1 += 1; /* increases register passed as "reg1" by 1. */
    
    SETZSP(value); /* set ZSP flags */
}

inline void ChipData::DCR(int8 reg1)
{
    int8 value = reg1 - 1 - 0;

    this->ac = CARRY(reg1, 1, 0, 4); 
    reg1 -= 1; /* increases register passed as "reg1" by 1. */
    
    SETZSP(value); /* set ZSP flags */
}

void ChipData::emu_init(void)
{
    // set all needed vars to starting vals
    this->sp = 0xF000;
    this->pc = 0;
    this->cycle = 0;
    this->opcode = 0;
    
    // unsure if we need to reset mem of memory or just delete it, this may causes errors
    memset(memory.data(), 0, 0xFFFF);

    // registers
    this->regA = 0;
    this->regBC = 0;
    this->regDE = 0;
    this->regHL = 0;
 
    // flags
    this->s = 0;
    this->z = 0;
    this->p = 0;
    this->c = 0;
    this->ac = 0;

    // closes emulator when true
    this->halt = false;

    std::cout << "INIT SUCCESS!!! " << std::endl; 
}

bool ChipData::load_rom(std::string rom)
{ 
    // if file is nothing then:
    std::ifstream file(rom, std::ios::in | std::ios::binary | std::ios::ate);
    
    if(!file.is_open())
    {
        std::cout << "Error: " << strerror(errno) << std::endl;
        return false; 
    }
    
    /* find total size of the rom file */
    file.seekg(0, std::ios::end);
    int16 size = file.tellg();
    file.seekg(0, std::ios::beg);
   
    // check if rom is smaller enough to fit into memory
    if(size < (this->memory.size() - 0x100))
    {
        // read ROM into memory array at the address 0x100
        file.read((char*)&this->memory[0x100],size);
        std::cout << "[+] ROM loaded successfully!" << std::endl;
        std::cout << "[+] Loaded " << size << " bytes into memory." << std::endl;
        
        // prints all of memory array
        //std::copy(std::begin(this->memory), std::end(this->memory),
        //    std::experimental::make_ostream_joiner(std::cout, " "));
        file.close();

        return true;
    }
    std::cout << "[+] ROM too big, returning..." << std::endl;
    file.close();

    return false;
}

void ChipData::emulate_cycle(void)
{
    // https://www.pastraiser.com/cpu/i8080/i8080_opcodes.html
    // https://github.com/duongvuhong/intel8080 this is the best documentaion ever, thank you who ever wrote this :)
    cycle++;
    opcode = GrabByte(pc);

    //std::cout << opcode <<std::endl;
    if(halt)
    {
        // TODO change this with a nice shutdown sequence or change func return type to bool and just return false.
        exit(1);
    }

    switch(opcode)
    {
        case 0x00: /* Not Documented, NOP */
        case 0x08:
        case 0x10:
        case 0x18:
        case 0x20:
        case 0x28:  
        case 0x30:
        case 0x38:
            break;

        /* LXI */
        case 0x01: this->regBC = ReadWord(pc); this->pc += 2; /* LXI B */
        case 0x11: this->regDE = ReadWord(pc); this->pc += 2; /* LXI D */
        case 0x21: this->regHL = ReadWord(pc); this->pc += 2; /* LXI H */
        case 0x31: this->sp = ReadWord(pc); this->pc += 2; /* LXI SP */

        /* STAX */
        case 0x02: PushByte(regBC, *regA); break; /* STAX B */
        case 0x12: PushByte(regDE, *regA); break; /* STAX D */

        /* SHLD */
        case 0x22: PushWord(ReadWord(this->pc), regHL); this->pc += 2; break; /* SHLD */ 

        /* STA */
        case 0x32: PushByte(ReadWord(this->pc), *this->regA); this->pc += 2; break; /* STA */

        /* INX */ 
        case 0x03: this->regBC++; break; /* INX B */
        case 0x13: this->regDE++; break; /* INX D */
        case 0x23: this->regHL++; break; /* INX H */
        case 0x33: this->sp++; break; /* INX SP */ 

        /* INR */
        case 0x04: INR(*regB); break; /* INR B */
        case 0x0C: INR(*regC); break; /* INR C */
        case 0x14: INR(*regD); break; /* INR D */
        case 0x1C: INR(*regE); break; /* INR E */
        case 0x24: INR(*regH); break; /* INR H */
        case 0x2C: INR(*regL); break; /* INR L */
        case 0x34: INR(*regM); break; /* INR M */
        case 0x3C: INR(*regA); break; /* INR A */

        /* DCR */ 
        case 0x05: DCR(*regB); break; /* DCR B */ 
        case 0x0D: DCR(*regC); break; /* DCR C */
        case 0x15: DCR(*regD); break; /* DCR D */
        case 0x1D: DCR(*regE); break; /* DCR E */
        case 0x25: DCR(*regH); break; /* DCR H */
        case 0x2D: DCR(*regL); break; /* DCR L */
        case 0x35: DCR(*regM); break; /* DCR M */
        case 0x3D: DCR(*regA); break; /* DCR A */

        /* MVI */
        case 0x06: *this->regB = GrabByte(pc); break; /* MVI B */
        case 0x0E: *this->regC = GrabByte(pc); break; /* MVI C */
        case 0x16: *this->regD = GrabByte(pc); break; /* MVI D */
        case 0x1E: *this->regE = GrabByte(pc); break; /* MVI E */
        case 0x26: *this->regH = GrabByte(pc); break; /* MVI H */
        case 0x2E: *this->regL = GrabByte(pc); break; /* MVI L */
        case 0x36: *this->regM = GrabByte(pc); break; /* MVI M */
        case 0x3E: *this->regA = GrabByte(pc); break; /* MVI A */

        /* SPECIAL */
        case 0x07: this->c = *this->regA >> 7; *this->regA = (*this->regA << 1) | this->c; break; /* RLC rotate regA to the left*/
        case 0x0F: this->c = *this->regA & 1; *this->regA = (*this->regA >> 1) | (this->c << 7); break; /* RRC rotate regA to the right*/
        case 0x17: this->c = *this->regA >> 7; *this->regA = (*this->regA << 1) | this->c; break; /* RAL rotate regA to the left with carry */
        case 0x1F: this->c = *this->regA & 1; *this->regA = (*this->regA >> 1) | (this->c << 7); break; /* RAR rotate regA to the left with carry */
        case 0x27: break; /* i'm not too sure about this one so i think i'll come back to it once i have got the emulator up and running. !!!TODO!!! */
        case 0x2F: *this->regA = ~*this->regA; break; /* CMA */
        case 0x37: this->c = 1; break; /* STC */
        case 0x3F: this->c = !this->c; break; /* CMC */

        /* DAD */
        case 0x09: this->c = (this->regHL + *this->regB) > 0xFFFF; this->regHL += this->regBC; break; /* DAD B */
        case 0x19: this->c = (this->regHL + this->regDE) > 0xFFFF; this->regHL += this->regDE; break; /* DAD D */
        case 0x29: this->c = (this->regHL + this->regHL) > 0xFFFF; this->regHL += this->regHL; break; /* DAD H */
        case 0x39: this->c = (this->regHL + this->sp) > 0xFFFF; this->regHL += this->sp; break; /* DAD SP */

        /* LHLD */
        case 0x2A: this->regHL = ReadWord(ReadWord(pc)); this->pc += 2; break; /* LHLD */

        /* LDAX */ 
        case 0x0A: *this->regA = GrabByte(regBC); break; /* LDAX B */
        case 0x1A: *this->regA = GrabByte(regDE); break; /* LDAX D */ 

        /* LDA */
        case 0x3A: *this->regA = GrabByte(ReadWord(pc)); this->pc += 2; break; /* LDA */ 

        /* DCX */
        case 0x0B: this->regBC--; break; /* DCX B */
        case 0x1B: this->regDE--; break; /* DCX D */
        case 0x2B: this->regHL--; break; /* DCX H */
        case 0x3B: this->sp--; break; /* DCX SP */

        /* MOV */
        case 0x40: *this->regB = *this->regB; break; /* MOV B,B */
        case 0x41: *this->regB = *this->regC; break; /* MOV B,C */
        case 0x42: *this->regB = *this->regD; break; /* MOV B,D */
        case 0x43: *this->regB = *this->regE; break; /* MOV B,E */
        case 0x44: *this->regB = *this->regH; break; /* MOV B,H */
        case 0x45: *this->regB = *this->regL; break; /* MOV B,L */
        case 0x46: *this->regB = *this->regM; break; /* MOV B,M */
        case 0x47: *this->regB = *this->regA; break; /* MOV B,A */
        
        case 0x48: *this->regC = *this->regB; break; /* MOV C,B */
        case 0x49: *this->regC = *this->regC; break; /* MOV C,C */
        case 0x4A: *this->regC = *this->regD; break; /* MOV C,D */
        case 0x4B: *this->regC = *this->regE; break; /* MOV C,E */
        case 0x4C: *this->regC = *this->regH; break; /* MOV C,H */
        case 0x4D: *this->regC = *this->regL; break; /* MOV C,L */
        case 0x4E: *this->regC = *this->regM; break; /* MOV C,M */
        case 0x4F: *this->regC = *this->regA; break; /* MOV C,A */

        case 0x50: *this->regD = *this->regB; break; /* MOV D,B */
        case 0x51: *this->regD = *this->regC; break; /* MOV D,C */
        case 0x52: *this->regD = *this->regD; break; /* MOV D,D */
        case 0x53: *this->regD = *this->regE; break; /* MOV D,E */
        case 0x54: *this->regD = *this->regH; break; /* MOV D,H */
        case 0x55: *this->regD = *this->regL; break; /* MOV D,L */
        case 0x56: *this->regD = *this->regM; break; /* MOV D,M */
        case 0x57: *this->regD = *this->regA; break; /* MOV D,A */
        
        case 0x58: *this->regE = *this->regB; break; /* MOV E,B */
        case 0x59: *this->regE = *this->regC; break; /* MOV E,C */
        case 0x5A: *this->regE = *this->regD; break; /* MOV E,D */
        case 0x5B: *this->regE = *this->regE; break; /* MOV E,E */
        case 0x5C: *this->regE = *this->regH; break; /* MOV E,H */
        case 0x5D: *this->regE = *this->regL; break; /* MOV E,L */
        case 0x5E: *this->regE = *this->regM; break; /* MOV E,M */
        case 0x5F: *this->regE = *this->regA; break; /* MOV E,A */
            
        case 0x60: *this->regH = *this->regB; break; /* MOV H,B */
        case 0x61: *this->regH = *this->regC; break; /* MOV H,C */
        case 0x62: *this->regH = *this->regD; break; /* MOV H,D */
        case 0x63: *this->regH = *this->regE; break; /* MOV H,E */
        case 0x64: *this->regH = *this->regH; break; /* MOV H,H */
        case 0x65: *this->regH = *this->regL; break; /* MOV H,L */
        case 0x66: *this->regH = *this->regM; break; /* MOV H,M */
        case 0x67: *this->regH = *this->regA; break; /* MOV H,A */
        
        case 0x68: *this->regL = *this->regB; break; /* MOV L,B */
        case 0x69: *this->regL = *this->regC; break; /* MOV L,C */
        case 0x6A: *this->regL = *this->regD; break; /* MOV L,D */
        case 0x6B: *this->regL = *this->regE; break; /* MOV L,E */
        case 0x6C: *this->regL = *this->regH; break; /* MOV L,H */
        case 0x6D: *this->regL = *this->regL; break; /* MOV L,L */
        case 0x6E: *this->regL = *this->regM; break; /* MOV L,M */
        case 0x6F: *this->regL = *this->regA; break; /* MOV L,A */

        case 0x70: *this->regM = *this->regB; break; /* MOV M,B */
        case 0x71: *this->regM = *this->regC; break; /* MOV M,C */
        case 0x72: *this->regM = *this->regD; break; /* MOV M,D */
        case 0x73: *this->regM = *this->regE; break; /* MOV M,E */
        case 0x74: *this->regM = *this->regH; break; /* MOV M,H */
        case 0x75: *this->regM = *this->regL; break; /* MOV M,L */
        
        case 0x76: this->halt = true; break; /* HLT */

        case 0x77: *this->regM = *this->regA; break; /* MOV M,A */
        
        case 0x78: *this->regA = *this->regB; break; /* MOV M,B */
        case 0x79: *this->regA = *this->regC; break; /* MOV A,C */
        case 0x7A: *this->regA = *this->regD; break; /* MOV A,D */
        case 0x7B: *this->regA = *this->regE; break; /* MOV A,E */
        case 0x7C: *this->regA = *this->regH; break; /* MOV A,H */
        case 0x7D: *this->regA = *this->regL; break; /* MOV A,L */
        case 0x7E: *this->regA = *this->regM; break; /* MOV A,M */
        case 0x7F: *this->regA = *this->regA; break; /* MOV A,A */

        /* ADD */
        case 0x80: ADD(*regA, *regB, 0); *regA += *regB; break; /* ADD B */
        case 0x81: ADD(*regA, *regC, 0); *regA += *regC; break; /* ADD C */
        case 0x82: ADD(*regA, *regD, 0); *regA += *regD; break; /* ADD D */
        case 0x83: ADD(*regA, *regE, 0); *regA += *regE; break; /* ADD E */
        case 0x84: ADD(*regA, *regH, 0); *regA += *regH; break; /* ADD H */
        case 0x85: ADD(*regA, *regL, 0); *regA += *regL; break; /* ADD L */
        case 0x86: ADD(*regA, *regM, 0); *regA += *regM; break; /* ADD M */
        case 0x87: ADD(*regA, *regA, 0); *regA += *regA; break; /* ADD A */

        /* ACD */ 
        /* Here the third parameter of the ADD function is the carry flag (c) */
        case 0x88: ADD(*regA, *regB, c); break; /* ADC B */
        case 0x89: ADD(*regA, *regC, c); break; /* ADC C */
        case 0x8A: ADD(*regA, *regD, c); break; /* ADC D */
        case 0x8B: ADD(*regA, *regE, c); break; /* ADC E */
        case 0x8C: ADD(*regA, *regH, c); break; /* ADC H */
        case 0x8D: ADD(*regA, *regL, c); break; /* ADC L */
        case 0x8E: ADD(*regA, *regM, c); break; /* ADC M */
        case 0x8F: ADD(*regA, *regA, c); break; /* ADC A */

        /* SUB */
        case 0x90: SUB(*regA, *regB, 0); *regA -= *regB; break; /* SUB B */
        case 0x91: SUB(*regA, *regC, 0); *regA -= *regC; break; /* SUB C */
        case 0x92: SUB(*regA, *regD, 0); *regA -= *regD; break; /* SUB D */
        case 0x93: SUB(*regA, *regE, 0); *regA -= *regE; break; /* SUB E */
        case 0x94: SUB(*regA, *regH, 0); *regA -= *regH; break; /* SUB H */
        case 0x95: SUB(*regA, *regL, 0); *regA -= *regL; break; /* SUB L */
        case 0x96: SUB(*regA, *regM, 0); *regA -= *regM; break; /* SUB M */
        case 0x97: SUB(*regA, *regA, 0); *regA -= *regA; break; /* SUB A */

        /* SBB */ 
        /* Here the third parameter of the SUB function is the carry flag (c) */
        case 0x98: SUB(*regA, *regB, c); break; /* SBB B */
        case 0x99: SUB(*regA, *regC, c); break; /* SBB C */
        case 0x9A: SUB(*regA, *regD, c); break; /* SBB D */
        case 0x9B: SUB(*regA, *regE, c); break; /* SBB E */
        case 0x9C: SUB(*regA, *regH, c); break; /* SBB H */
        case 0x9D: SUB(*regA, *regL, c); break; /* SBB L */
        case 0x9E: SUB(*regA, *regM, c); break; /* SBB M */
        case 0x9F: SUB(*regA, *regA, c); break; /* SBB A */

        /* ANA */
        case 0xA0: ANA(*regB); break; /* ANA B */
        case 0xA1: ANA(*regC); break; /* ANA C */
        case 0xA2: ANA(*regD); break; /* ANA D */
        case 0xA3: ANA(*regE); break; /* ANA E */
        case 0xA4: ANA(*regH); break; /* ANA H */
        case 0xA5: ANA(*regL); break; /* ANA L */
        case 0xA6: ANA(*regM); break; /* ANA M */
        case 0xA7: ANA(*regA); break; /* ANA A */

        /* XRA */
        case 0xA8: XRA(*regB); break; /* XRA B */
        case 0xA9: XRA(*regC); break; /* XRA C */
        case 0xAA: XRA(*regD); break; /* XRA D */
        case 0xAB: XRA(*regE); break; /* XRA E */
        case 0xAC: XRA(*regH); break; /* XRA H */
        case 0xAD: XRA(*regL); break; /* XRA L */
        case 0xAE: XRA(*regM); break; /* XRA M */
        case 0xAF: XRA(*regA); break; /* XRA A */

        /* ORA */
        case 0xB0: ORA(*regB); break; /* ORA B */
        case 0xB1: ORA(*regC); break; /* ORA C */
        case 0xB2: ORA(*regD); break; /* ORA D */
        case 0xB3: ORA(*regE); break; /* ORA E */
        case 0xB4: ORA(*regH); break; /* ORA H */
        case 0xB5: ORA(*regL); break; /* ORA L */
        case 0xB6: ORA(*regM); break; /* ORA M */
        case 0xB7: ORA(*regA); break; /* ORA A */

        /* CMP */
        case 0xB8: SUB(*regA, *regB, 0); break; /* CMP B */
        case 0xB9: SUB(*regA, *regC, 0); break; /* CMP C */
        case 0xBA: SUB(*regA, *regD, 0); break; /* CMP D */
        case 0xBB: SUB(*regA, *regE, 0); break; /* CMP E */
        case 0xBC: SUB(*regA, *regH, 0); break; /* CMP H */
        case 0xBD: SUB(*regA, *regL, 0); break; /* CMP L */
        case 0xBE: SUB(*regA, *regM, 0); break; /* CMP M */
        case 0xBF: SUB(*regA, *regA, 0); break; /* CMP A */

        /* CLEAN THIS UP WHEN ALL ARE DONE */
        case 0xC0: if(!this->z) {this->pc = pop(); this->cycle += 6;} break; /* RNZ */
        case 0xC1: this->regBC = pop(); break; /* POP B */
        case 0xC2: if(!this->z) {this->pc = ReadNextWord();} break; /* JNZ */
        case 0xC3: this->pc = ReadWord(pc); break; /* !!!JMP A!!! We only need to add this JMP case as the other one is undocumented (to my knowledge) */
        case 0xC4: if(!this->z) {CALL(); this->cycle += 6;} break; /* CNZ */
        case 0xC5: PushOnStack(regBC); break; /* PUSH B */
        case 0xC6: ADD(*regA, GrabByte(this->pc), 0); *regA += GrabByte(this->pc); break; /* ADI */
        case 0xC7: PushOnStack(pc); this->pc = 0x00; break; /* RST 0 */
        case 0xC8: if(this->z) {this->pc = pop(); this->cycle += 6;} break; /* RZ */
        case 0xC9: this->pc = pop(); break; /* RET */ 
        case 0xCA: if(this->z) {this->pc = ReadNextWord();} break; /* JZ */
        case 0xCB: break; /* Undocumented JMP */
        case 0xCC: if(this->z) {CALL(); this->cycle += 6;} break; /* CZ */
        case 0xCD: CALL(); break; /* CALL */
        case 0xCE: ADD(*regA, GrabByte(this->pc), this->c); break;/* ACI d8 */
        case 0xCF: PushOnStack(pc); this->pc = 0x08; break; /* RST 1 */

        /* CLEAN THIS UP WHEN ALL ARE DONE */
        case 0xD0: if(!this->c) {this->pc = pop(); this->cycle += 6;} break; /* RNC */
        case 0xD1: this->regDE = pop(); break; /* POP D */
        case 0xD2: if(!this->c) {this->pc = ReadNextWord();} break; /* JNC */
        case 0xD3: throw std::invalid_argument("Opcode 0xD3 OUT not added."); break; /* !!!TODO!!! OUT */
        case 0xD4: if(!this->c) {CALL(); this->cycle += 6;} break; /* CNC */
        case 0xD5: PushOnStack(regDE); break; /* PUSH D */
        case 0xD6: SUB(*regA, GrabByte(pc), 0); *regA -= GrabByte(pc); break; /* SUI */
        case 0xD7: PushOnStack(pc); this->pc = 0x10; break; /* RST 2 */
        case 0xD8: if(this->c) {this->pc = pop(); this->cycle += 6;} break; /* RC */
        case 0xD9: break; /* Undocumented. Couldn't find anything about it xD */
        case 0xDA: if(this->c) {this->pc = ReadNextWord();} break; /* JC */
        case 0xDB: throw std::invalid_argument("Opcode 0xDB IN not added."); break; /* !!!TODO!!! IN */
        case 0xDC: if(this->c) {CALL(); this->cycle += 6;} break; /* CC */
        case 0xDD: break; /* Undocumented. Couldn't find anything about it */
        case 0xDE: SUB(*regA, GrabByte(this->pc), this->c); break; /* SBI d8 */
        case 0xDF: PushOnStack(pc); this->pc = 0x18; break; /* RST 3 */

        /* CLEAN THIS UP WHEN ALL ARE DONE */
        case 0xE0: if(!this->p) {this->pc = pop(); this->cycle += 6;} break; /* RPO */
        case 0xE1: this->regHL = pop(); break; /* POP H */
        case 0xE2: if(this->p) {this->pc = ReadNextWord();} break; /* JPO */
        case 0xE3: pop(); PushOnStack(regHL); break; /* XTHL */
        case 0xE4: if(this->p) {CALL(); this->cycle += 6;} break; /* CPO */
        case 0xE5: PushOnStack(regHL); break; /* PUSH H */
        case 0xE6: ANA(GrabByte(this->pc)); break; /* ANI d8 */
        case 0xE7: PushOnStack(pc); this->pc = 0x20; break; /* RST 4 */
        case 0xE8: if(this->p) {this->pc = pop(); this->cycle += 6;} break; /* RPE */
        case 0xE9: this->pc = this->regHL; break; /* PCHL */
        case 0xEA: if(this->p) {this->pc = ReadNextWord();} break; /* JPE */
        case 0xEB: this->regDE = this->regHL; break; /* XCHG */
        case 0xEC: if(this->p) {CALL(); this->cycle += 6;} break; /* CPE */
        case 0xED: break; /* Undocumented. Couldn't find anything about it */
        case 0xEE: *this->regA ^= GrabByte(pc); ADD(*regA, 0, 0); break; /* XRI */
        case 0xEF: PushOnStack(pc); this->pc = 0x28; break; /* RST 5 */

        /* CLEAN THIS UP WHEN ALL ARE DONE */
        case 0xF0: if(!this->s) {this->pc = pop(); this->cycle += 6;} break; /* RP */
        case 0xF1: popPSW(); break; /* POP PSW */
        case 0xF2: if(!this->s) {this->pc = ReadNextWord();} break;
        case 0xF3: /* not actually sure what this is */ break;
        case 0xF4: if(!this->s) {CALL(); this->cycle += 6;} break; /* CP */
        case 0xF5: pushPSW(); break; /* PUSH PSW */
        case 0xF6: *this->regA |= GrabByte(pc); ADD(*regA, 0, 0); break; /* ORI */
        case 0xF7: PushOnStack(pc); this->pc = 0x30; break; /* RST 6 */
        case 0xF8: if(this->s) {this->pc = pop(); this->cycle += 6;} break; /* RM */
        case 0xF9: this->sp = this->regHL; break; /* SPHL */ 
        case 0xFA: if(this->s) {ReadNextWord();} break; /* JM */
        case 0xFB: break; /* EI Couldn't find anything about it */
        case 0xFC: if(this->s) {CALL();} break; /* CM */
        case 0xFD: break; /* Undocumented, Couldn't find anything about it */
        case 0xFE: SUB(*regA, GrabByte(pc), 0); break; /* CPI */
        case 0xFF: PushOnStack(pc); this->pc = 0x38; break; /* RST 6 */

        default:
            throw std::runtime_error("Unknown opcode. Breaking...\n"); 
            break;
    }


}

ChipData::ChipData(std::string rom)
{
    // call priv emu functions from class ChipData
    this->emu_init(); 
    this->load_rom(rom);
}

// deconstructor
ChipData::~ChipData()
{
    // TODO add nice shutdown sequence 
}
