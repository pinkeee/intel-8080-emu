TARGET = a.out
#linker
CXX=clang++
# debug
DEBUG=#-v
# optimisation
OPT=-O3
# warnings
WARN=#-Wall -Wextra -Werror -Wpedantic

PTHREAD=-pthread

CCFLAGS=$(DEBUG) $(OPT) $(WARN) $(PTHREAD) 

# linker
LDFLAGS=$(PTHREAD) -lSDL2 -lSDL2_mixer -std=c++17

all: src/*.*
	$(CXX) src/*.* $(CCFLAGS) $(LDFLAGS) 
clean:
	rm -f *.o src/*.0 src/*.*.gch $(TARGET)
