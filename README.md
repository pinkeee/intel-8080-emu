# intel-8080-emu
A simple Intel 8080 emulator written in C++

# TODO
- Every 10 seconds show what speed emulator running at (should be an easy add)
- Add images
- Add tick marks for what CPU test ROMS pass.
- A lot more too
- Think about adding support for games with SDL2 later down the line.
- Once emulator works and if I ever get round to adding SDL2 support (therefore game support), set up discord rich presence support?
